window.onload = init;

function init() {
	handleTempChange();
	document.getElementById("tempSlider").addEventListener("input", handleTempChange);
}


function handleTempChange() {
	document.getElementById("temp").innerHTML = document.getElementById("tempSlider").value;
}



function onClick() {
	const url = document.getElementById("api_root").value + document.getElementById("resource").value;
	const method = document.forms.apiform.req_type.value || "GET";
	document.getElementById("status").innerHTML = "Request made to '" + url + "' using method '" + method + "'";
	makeRequest(url, method);
}

async function makeRequest(url, method) {
	const post = await fetch(url).then((res) => res.json());
	document.getElementById("response").innerHTML = JSON.stringify(post, null, 2);
}

